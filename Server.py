from ast import If
import socket
import Client

class Server:
    def __init__(self, port_number):
        self.serverSocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.serverSocket.bind(("", port_number))
        self.portNumber = port_number
        self.isConnected = False

    def openConnection(self):
        self.isConnected = True
        message_box = []
        isPrinted = False
        while self.isConnected:
            message, address = self.serverSocket.recvfrom(256)

            if message is not None:

                receive_msg = Client.ReceiveMsg(message)

                if receive_msg.gettingSendReq():
                    self.sendingAcknowledge(address)
                else:
                  
                    packets_num = int.from_bytes(receive_msg.getPackageSize(), "big")
                  
                    if packets_num > 1:

                        packet_id = int.from_bytes(receive_msg.getPackID(), "big")

                        if len(message_box) == 0:
                            message_box.append(receive_msg)
                        else:

                            if packet_id == packets_num:
                                message_box.append(receive_msg)

                                validated, val_packets = self.verifyPacks(message_box, address, checkNumPacks=True)
                                if validated:
                                    new_packets = self.addNewPacks(val_packets, message_box, packets_num)
                                    message_box = []
                                    val_packets = []
                                    isPrinted = False

                                else:
                                    print("\nError: Error occured during receiving.")
                                    print("\nEnter your message: ")
                                    message_box = []
                                    val_packets = []
                                    isPrinted = False

                            else:
                                message_box.append(receive_msg)
                    else:
                        message_box.append(receive_msg)
                      
                        validated, val_packets = self.verifyPacks(message_box, address)
                        if validated:
                            print("\nYou received message:")
                            print(receive_msg.caeser_decoder())
                            print("\nEnter your message: ")
                            self.sendingAcknowledge(address)
                            val_packets = []
                            message_box = []

                        else:
                            print("\nError: Error occured during receiving.")
                            print("\nEnter your message: ")
                            val_packets = []
                            message_box = []

    def closeConnection(self):
      self.isConnected = False
      self.serverSocket.close()

    def changePort(self, newPort):
      self.closeConnection()
      self.serverSocket = None
      self.portNumber = newPort
      self.serverSocket = socket(socket.AF_INET, socket.SOCK_DGRAM)
      self.serverSocket.bind(("", newPort))

    def sendingAcknowledge(self, address):
        ack = bytes("ACK", "utf-8")
        check = (3).to_bytes(2, "big")
        packet_id = (1).to_bytes(2, "big")
        total_pack = (1).to_bytes(2, "big")
        
        pack = packet_id + total_pack + check + ack
        self.serverSocket.sendto(pack, address)
      
    def testCheckSum(self, check, msg):
        
        msg_len = len(msg)
        
        b_check = msg_len % 99

        if b_check == 0 and msg_len != 0:
            b_check = 99

        if b_check == check:
            checking = True
        else:
            checking = False

        return checking

    def verifyPacks(self, packets, address, checkNumPacks=False):

        req_packs = []
        packet_value = []
        packet_ids = []

        validated = False

        for p in packets:
            check = int.from_bytes(p.getCheckSum(), "big")
            packet_id = int.from_bytes(p.getPackID(), "big")
            msg = p.getData()
            packet_ids.append(packet_id)

            if not self.testCheckSum(check, msg):
                req_packs.append(packet_id)

        if checkNumPacks:

            total_packet = []
            for i in range(0, len(packets)):
                total_packet.append(int.from_bytes(packets[i].getPackageSize(),"big"))

            if total_packet.count(total_packet[0]) == len(total_packet):

                if len(packets) != total_packet[0]:
                    for i in range(1, total_packet[0] + 1):
                        if i not in packet_ids:
                            req_packs.append(i)

                    packet_value = self.requestPackets(req_packs, address)
                    if packet_value is not None:
                        validated = True

                elif len(packets) == total_packet[0]:

                    validated = True
                    packet_value = packets
            else:
                packet_value = self.requestPackets(req_packs, address)
                if packet_value is not None:
                    validated = True

        else:
            if len(req_packs) == 0:
                validated = True
                packet_value = packets

        return validated, packet_value

    def requestPackets(self, requested, address):
        print("requesting packets")

        msg = bytes("011100000110111101100111", "utf-8")
        check = (24).to_bytes(2, "big")
        packet_id = (1).to_bytes(2, "big")
        total_pack = (1).to_bytes(2, "big")
        header = packet_id + total_pack + check 

        pack = header + msg

        self.serverSocket.sendto(pack, address)

        self.serverSocket.settimeout(1)
        new_packs = []

        try:
            data, address_serv = self.serverSocket.recvfrom(256)
        except timeout as inst:
            print("Rec ack timout, retransmit")
        else:

            rec_msg = Client.ReceiveMsg(data)

            if rec_msg.gettingAck():
                self.serverSocket.sendto(self.createReqPacket(requested), address)

                print("listening")
                for i in requested:
                    try:
                        data, address = self.serverSocket.recvfrom(256)
                    except timeout as inst:
                        print("socket timeout")
                    else:
                        new_packet = Client.ReceiveMsg(data)
                        new_packs.append(new_packet)

            else:
                print("unexpected error. try again later")

        self.serverSocket.settimeout(None)
        print(new_packs)
        return new_packs

    def addNewPacks(self, newPacks, packs, totalPacks):
        total_packets_arr = []
        for i in range(totalPacks):
            print(i)
            for p in packs:
                if (int.from_bytes(p.getPackID(), "big")) == i + 1:
                    total_packets_arr.append(p)

            for p in newPacks:
                if (int.from_bytes(p.getPackID(), "big")) == i + 1:
                    total_packets_arr.append(p)

        print(total_packets_arr)
        return total_packets_arr

