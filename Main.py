from Client import Client
import socket

import multiprocessing


if __name__ == '__main__':


    run_state = True
    UDP_IP_ADDRESS = "127.0.0.1"
    UDP_PORT_NO = 12000

    print("""
    You have to input the IP address.
    Format: X.X.X.X
    If you input nothing, the target IP will be 127.0.0.1.
    """)
    new_IP = input("Enter the Target IP address: ")
    if new_IP != '':
        UDP_IP_ADDRESS = new_IP

    localPortNum = input("Enter port number for chatting(Input Nothing: Default Port)-> ")

    if localPortNum != '':
        UDP_PORT_NO = int(localPortNum)
    client_proc = Client(UDP_PORT_NO)

    listen_proc = multiprocessing.Process(target=client_proc.openReceiveConnection)
    listen_proc.start()

    print("""
        Welcome to SMP
    From now on You can chat.
To exit, press q then Hit the Enter Key.
            """)
    
    while run_state:
        user_msg = input("Enter your message: ")

        if user_msg == 'q' or user_msg == 'Q':
            run_state = False
        else:
            client_proc.sendMessage(user_msg, UDP_IP_ADDRESS, UDP_PORT_NO)

    listen_proc.terminate()
    listen_proc.join()

    client_proc.internalServer.closeCon()
