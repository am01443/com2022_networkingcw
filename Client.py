import socket
import sys
import threading
from Server import Server

class Client:
    def __init__(self, portNum):

        self.clientSocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

        self.internalServer = Server(portNum)
        self.buffer = 256

    def getPortNum(self):
     self.internalServer.getPortNum()

    def changeLocalP(self,newPort):
        self.internalServer.changePort(newPort)

    def openReceiveConnection(self):
        self.internalServer.openConnection()

    def closeRecConnection(self):
        self.internalServer.closeCon()

    def sendMessage(self, message, targetIP, targetPortNum):

        if self.handshaking(targetIP, targetPortNum):

            packets = self.partitionMsg(message)

            for packet in packets:
                self.clientSocket.sendto(packet, (targetIP, targetPortNum))

            self.clientSocket.settimeout(1)
            try:
                data, address = self.clientSocket.recvfrom(256)

            except socket.timeout as inst:
                print("Receiving \"ACK\" Timeout, Resend!")

            else:
                receive_msg = ReceiveMsg(data)
                if receive_msg.gettingAck():
                    print("Received \"ACK\", All Message sent successfully")

                elif receive_msg.gettingResend():
                    print("Notification: Resend signal received")

                    self.internalServer.sendAck(address)

                    try:
                        data, address = self.clientSocket.recvfrom(256)

                    except socket.timeout as inst:
                        print("Error: Request Timeout")

                    else:

                        receive_msg = ReceiveMsg(data)
                        msg_data = receive_msg.getData().decode("utf-8")
                        req_packs = msg_data.split(",")
                        repeat_pack_list = []

                        for ID in req_packs:
                            repeat_pack_list.append(packets[int(ID) - 1])

                        for p in repeat_pack_list:
                            self.clientSocket.sendto(p, address)

    def createHeader(self, packID, total, check):
        s_header = packID.to_bytes(2, "big") + total.to_bytes(2, "big") + check.to_bytes(2, "big")
        return s_header

    def createChecksum(self, msg):
        check = len(msg)
        s_check = check % 99
        if check != 0 and s_check == 0:
            s_check = 99
        return s_check 

    def caeser_cipher(self, byte_msg, numPackets):
        plain_text = byte_msg.decode("utf-8")
        encrypted = ""
        key = (69 - numPackets) * 2
        for char in plain_text:
            if char.islower():
                char_index = ord(char) - ord('a')
                char_shifted = (char_index + key) % 26 + ord('a')
                char_new = chr(char_shifted)
                encrypted += char_new
            elif char.isupper(): 
                char_index = ord(char) - ord('A')
                char_shifted = (char_index + key) % 26 + ord('A')
                char_new = chr(char_shifted)
                encrypted += char_new
            elif char.isdigit():
                char_new = (int(char) + key) % 10
                encrypted += str(char_new)
            else:
                encrypted += char

        return bytes(encrypted, "utf-8")
    
    def handshaking(self, ip, port):

        print("Send Request")
        success_sending = False

        sending_msg = "011000100111010101101110011001000110000100001010"
        byte_msg = bytes("011000100111010101101110011001000110000100001010", "utf-8")

        pack = self.createPacket(byte_msg, 1, 1)

        self.clientSocket.settimeout(1)
        for i in range(1, 5):
            
            try:
                self.clientSocket.sendto(pack, (ip, port))

                reply, server_address_info = self.clientSocket.recvfrom(256)
                receive_msg = ReceiveMsg(reply)
                if receive_msg.gettingAck():
                    print("Acknowledgement")
                    pass
                else:
                    raise socket.timeout

            except socket.timeout as inst:
                if i < 5:
                    print("Trying again")
                else:
                    print("Failed to establish a connection")

            else:
                success_sending = True
                break

        return success_sending

    def partitionMsg(self, msg):

        return_packets = []

        byte_msg = msg.encode("utf-8")
        if len(byte_msg) <= 246:
            return_packets.append(self.createPacket(byte_msg, 1, 1))
        else:
            part_msg = []
            len_count = 0

            while len_count < len(byte_msg):

                is_space = False
                count = 246

                while not is_space:
                    if len_count + count > len(byte_msg):
                        part_msg.append(byte_msg[len_count:])
                        is_space = True
                        len_count += count
                    else:
                        if msg[(len_count + (count - 1))] == " ":
                            part_msg.append(byte_msg[len_count: (len_count + count)])
                            len_count += count
                            is_space = True
                        elif len_count + count == len(byte_msg):
                            part_msg.append(byte_msg[len_count: (len_count + count)])
                            len_count += count
                            is_space = True
                        else:
                            count -= 1

            for i in range(len(part_msg)):
                return_packets.append(self.createPacket(part_msg[i], i+1, len(part_msg)))
        return return_packets

    def createPacket(self, msg, packId, total):


        checksum = self.createChecksum(msg)
        header = self.createHeader(packId, total, checksum)
       
        enc_msg = self.caeser_cipher(msg, total)
        packet = header + enc_msg

        return packet

class ReceiveMsg:

    def __init__(self, message): 
        self.packID = message[0:2]
        self.packageSize = message[2:4]
        self.checkSum = message[4:6]
        self.data = message[6:]

    def getPackID(self):
        return self.packID

    def getPackageSize(self):
        return self.packageSize

    def getCheckSum(self):
        return self.checkSum
    
    def getData(self):
        return self.data

    def caeser_decoder(self):
        numPackets = int.from_bytes(self.getPackageSize(), "big")
        byte_msg = self.data

        key = (69 - numPackets) * 2
        ciphertext = byte_msg.decode()
        decrypted = ""
        for char in ciphertext:
            if char.islower():
                char_index = ord(char) - ord('a')
                char_origin_pos = (char_index - key) % 26 + ord('a')
                char_origin = chr(char_origin_pos)
                decrypted += char_origin
            elif char.isupper():
                char_index = ord(char) - ord('A')
                char_origin_pos = (char_index - key) % 26 + ord('A')
                char_origin = chr(char_origin_pos)
                decrypted += char_origin
            elif char.isdigit():
                char_origin = (int(char) - key) % 10
                decrypted += str(char_origin)
            else:
                decrypted += char

        return decrypted
    
    def gettingResend(self):
        msg = bytes("011100000110111101100111", "utf-8")
        if self.data == msg or self.caeser_decoder() == msg:
            return True
        else:
            return False

    def gettingSendReq(self):
        try:
            if self.data == b"011000100111010101101110011001000110000100001010" or self.caeser_decoder() == "011000100111010101101110011001000110000100001010":
                return True
            else:
                return False
        except Exception as e:
            return False

    def gettingAck(self):
        if self.data == b"ACK" or (self.caeser_decoder() == "ACK"):
            return True
        else:
            return False
